using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Simulab.Models;
using Microsoft.AspNetCore.Authorization;
using Simulab.Models.Database_Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Identity;
using System.Text.RegularExpressions;

namespace Simulab.Controllers
{
    [Authorize]
    public class ModelController : Controller
    {
        private SimulabContext _context = new SimulabContext();

        private readonly UserManager<UserModel> _userManager;
        public ModelController(UserManager<UserModel> userManager)
        {
            _userManager = userManager;
        }

        /// <summary>
        /// Create Model
        /// </summary>
        /// <param name="m">Model Object</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(ModelItemModel m)
        { 
            m.CreatorId = _userManager.GetUserId(User);
            m.CreationTime = DateTime.UtcNow;
            m.Structure = JsonConvert.SerializeObject(GetModelStructure(m.Equations));
            m.InitialConditions = JsonConvert.SerializeObject(GetInitialConditions(m.Equations));
            m.Disabled = false;
            if (!String.IsNullOrEmpty(m.Name) && !String.IsNullOrEmpty(m.Description) && !String.IsNullOrEmpty(m.Equations))
            {
                _context.ModelTable.Add(m);
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("MView", "Model");
        }

        public async Task<IActionResult> Edit(int id)
        {
            ModelItemModel model = await _context.ModelTable.FindAsync(id);
            return PartialView("Create", model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ModelItemModel m)
        {
            ModelItemModel model = new ModelItemModel
            {
                CreatorId = _userManager.GetUserId(User),
                Name = m.Name,
                Description = m.Description,
                CreationTime = DateTime.UtcNow,
                Equations = m.Equations,
                Structure = JsonConvert.SerializeObject(GetModelStructure(m.Equations)),
                InitialConditions = JsonConvert.SerializeObject(GetInitialConditions(m.Equations)),
                PreviousChange = m,
                PreviousChangeId = m.Id
            };

            _context.Entry(m).State = EntityState.Unchanged;
            _context.ModelTable.Add(model);

            await _context.SaveChangesAsync();

            return RedirectToAction("MView", "Model");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            ModelItemModel m = await _context.ModelTable.FirstOrDefaultAsync(x => x.Id == id && x.CreatorId == _userManager.GetUserId(User));
            if (m != null)
            {
                m.Disabled = true;
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("MView");
        }

        /// <summary>
        /// Return Model list page
        /// </summary>
        /// <returns></returns>
        public IActionResult List()
        {
            return View();
        }

        /// <summary>
        /// Return a list of models on page
        /// </summary>
        /// <returns></returns>
        [Route("~/Model/View")]
        public async Task<IActionResult> MView()
        {
            return View(await GetModelList(_userManager.GetUserId(User)));
        }

        /// <summary>
        /// return a list of models
        /// </summary>
        /// <returns></returns>
        public async Task<List<ModelItemModel>> GetModelList(string UserId)
        {
            List<ModelItemModel> response = _context.ModelTable.Where(x => x.CreatorId == UserId)
                  .OrderBy(x => x.Id).ToList();
            List<ModelItemModel> ret = response.ToList();
            foreach (ModelItemModel m in response)
            {
                if (m.PreviousChangeId != null)
                {
                    ret.Remove(ret.FirstOrDefault(x => x.Id == m.PreviousChangeId));
                }
            }
            ret = ret.ToList().Where(x => !x.Disabled).ToList();
            return ret;
        }

        /// <summary>
        /// Generate single model view
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Detail(int id)
        {
            ModelItemModel m = await _context.ModelTable.FirstOrDefaultAsync(x => x.Id == id);
            return PartialView(m);

        }

        /// <summary>
        /// Get initial conditions from equations
        /// </summary>
        /// <param name="Equations"></param>
        /// <returns></returns>
        private List<string> GetInitialConditions(string Equations)
        {
            Dictionary<string, List<string>> rawData = GetRawDataStructure(Equations);

            List<string> ret = new List<string>();

            foreach (KeyValuePair<string, List<string>> entry in rawData)
            {
                ret.Add(entry.Key);
            }
            return ret.Distinct().ToList();
        }

        /// <summary>
        /// Get model structure for vis.js
        /// </summary>
        /// <param name="Equations"></param>
        /// <returns></returns>
        private List<List<object>> GetModelStructure(string Equations)
        {
            Dictionary<string, List<string>> rawData = GetRawDataStructure(Equations);

            List<List<object>> ret = new List<List<object>>();

            List<string> uniqueNodes = GetUniqueNodes(rawData);

            List<object> nodes = GetNodes(uniqueNodes);

            List<object> edges = GetEdges(rawData, uniqueNodes);

            ret.Add(nodes);
            ret.Add(edges);

            return ret;
        }

        #region Vis formatter

        #region Nodes
        /// <summary>
        /// Get Raw data structure for Vis.js using RegEx to find all variables
        /// </summary>
        /// <param name="Equations"></param>
        /// <returns></returns>
        private Dictionary<string, List<string>> GetRawDataStructure(string Equations)
        {
            Dictionary<string, List<string>> ret = new Dictionary<string, List<string>>();
            List<string> EquationList = Equations.Replace("\r", "").Split("\n").ToList();
            Regex re = new Regex(@"([_a-zA-Z]+[0-9]*)*(?![a-z]*\()");

            //Match all variables
            foreach (string s in EquationList)
            {
                if (!String.IsNullOrEmpty(s))
                {
                    string processedEquation = s;
                    if (s.Contains('%'))
                    {
                        int index = s.IndexOf('%');
                        if (index == 0)
                        {
                            continue;
                        }
                        processedEquation = s.Substring(0, s.IndexOf('%'));
                    }
                    if (!String.IsNullOrEmpty(processedEquation))
                    {
                        string variable = String.Empty;
                        List<string> temp = new List<string>();
                        MatchCollection mc = re.Matches(processedEquation);
                        foreach (Match m in mc)
                        {
                            if (m.Success && !String.IsNullOrEmpty(m.Value))
                            {
                                if (String.IsNullOrEmpty(variable))
                                {
                                    variable = m.Value;
                                }
                                else
                                {
                                    if (!temp.Contains(m.Value))
                                    {
                                        temp.Add(m.Value);
                                    }
                                }
                            }
                        }
                        if (temp.Count > 0)
                        {
                            ret.Add(variable, temp);
                        }
                    }
                }

            }
            return ret;
        }

        /// <summary>
        /// Get unique variables including ICs
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        private List<string> GetUniqueNodes(Dictionary<string, List<string>> rawData)
        {
            List<string> ret = new List<string>();
            foreach (KeyValuePair<string, List<string>> entry in rawData)
            {
                ret.Add(entry.Key);
                ret.AddRange(entry.Value);
            }
            return ret.Distinct().ToList();
        }

        /// <summary>
        /// Get Node object for Vis.js
        /// </summary>
        /// <param name="UniqueNodes"></param>
        /// <returns></returns>
        private List<object> GetNodes(List<string> UniqueNodes)
        {
            List<object> ret = new List<object>();
            int index = 0;
            foreach (string s in UniqueNodes)
            {
                ret.Add(new { id = index, label = s, group = s });
                index++;
            }
            return ret;
        }
        #endregion

        #region Edges
        /// <summary>
        /// Get Edges object for Vis.js
        /// </summary>
        /// <param name="rawData"></param>
        /// <param name="UniqueNodes"></param>
        /// <returns></returns>
        private List<object> GetEdges(Dictionary<string, List<string>> rawData, List<string> UniqueNodes)
        {
            List<object> ret = new List<object>();
            foreach (KeyValuePair<string, List<string>> entry in rawData)
            {
                foreach (string s in entry.Value)
                {
                    ret.Add(new { from = UniqueNodes.IndexOf(s), to = UniqueNodes.IndexOf(entry.Key), arrows = "to" });
                }
            }
            return ret;
        }
        #endregion

        #endregion
    }
}