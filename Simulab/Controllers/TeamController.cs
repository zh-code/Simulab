﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Simulab.Models.Database_Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Simulab.Controllers
{
    [Authorize]
    public class TeamController : Controller
    {
        private SimulabContext _context = new SimulabContext();
        private readonly UserManager<UserModel> _userManager;

        public TeamController(UserManager<UserModel> userManager)
        {
            _userManager = userManager;
        }

        /// <summary>
        /// Return team create page
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Submit team create request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(TeamModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    UserModel user = await _context.UserTable.FirstOrDefaultAsync(x => x.UserName == User.Identity.Name);
                    TeamMemberModel teamPerm = new TeamMemberModel
                    {
                        R_Perm = 7,
                        W_Perm = 7,
                        X_Perm = 7,
                        User = user,
                        Team = model
                    };
                    _context.TeamMemberTable.Add(teamPerm);
                    model.Users = new List<TeamMemberModel>();
                    model.Users.Add(teamPerm);
                    await _context.Entry(user).Collection(c => c.Teams).LoadAsync();
                    user.Teams.Add(teamPerm);
                    await _context.SaveChangesAsync();
                }
                catch { ViewBag.Message = "Internal Error"; }
            }
            return RedirectToAction("Team", "User");
        }

        /// <summary>
        /// Return team update page
        /// </summary>
        /// <param name="teamId"></param>
        /// <returns></returns>
        public async Task<IActionResult> Update(int teamId)
        {
            TeamModel team = await _context.TeamTable.Where(x => x.Id == teamId)
                .Include(x => x.Users).FirstOrDefaultAsync();
            foreach (TeamMemberModel t in team.Users)
            {
                await _context.Entry(t).Reference(r => r.User).LoadAsync();
            }
            return PartialView(team);
        }

        /// <summary>
        /// Submit team update request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> SubmitUpdate(TeamModel model)
        {
            TeamModel t = await _context.TeamTable.FindAsync(model.Id);
            t.Name = model.Name;
            t.Description = model.Description;
            await _context.SaveChangesAsync();
            return RedirectToAction("Team", "User");
        }

        /// <summary>
        /// Return add team user page
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> AddUser()
        {
            return PartialView();
        }

        /// <summary>
        /// Submit add team user request
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddUser(TeamMemberModel m)
        {
            TeamMemberModel _currentUserPerm = await _context.TeamMemberTable.FirstOrDefaultAsync(_x => _x.User.UserName == User.Identity.Name);
            if (_currentUserPerm.R_Perm < m.R_Perm || _currentUserPerm.W_Perm < m.W_Perm || _currentUserPerm.X_Perm < m.X_Perm)
            {
                UserModel user = await _context.UserTable.Where(x => x.UserName.ToUpper() == m.User.UserName.ToUpper()).Include(x => x.Teams).FirstOrDefaultAsync();
                foreach (TeamMemberModel t in user.Teams)
                {
                    _context.Entry(t).Reference(x => x.Team).Load();
                }
                if (user.Teams.FirstOrDefault(x => x.Team.Id == m.Team.Id) != null)
                {
                    return StatusCode(409);
                }
                TeamModel team = await _context.TeamTable.FirstOrDefaultAsync(x => x.Id == m.Team.Id);
                try
                {
                    m.User = user;
                    m.Team = team;
                    await _context.TeamMemberTable.AddAsync(m);
                    await _context.SaveChangesAsync();
                    return Ok();
                }
                catch { }
                return StatusCode(500);
            }
            return Forbid();
        }

        /// <summary>
        /// Submit update team user request
        /// </summary>
        /// <param name="id"></param>
        /// <param name="r">Read permission</param>
        /// <param name="w">Write permission</param>
        /// <param name="x">Exec permission</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateUser(int id, int r, int w, int x)
        {
            TeamMemberModel m = await _context.TeamMemberTable.FirstOrDefaultAsync(_x => _x.Id == id);
            TeamMemberModel _currentUserPerm = await _context.TeamMemberTable.FirstOrDefaultAsync(_x => _x.User.UserName == User.Identity.Name);
            if (w < _currentUserPerm.W_Perm)
            {
                m.R_Perm = r;
                m.W_Perm = w;
                m.X_Perm = x;
                await _context.SaveChangesAsync();
                return Ok();
            }
            return Forbid();
        }

        /// <summary>
        /// Submit remove user from team request
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> RemoveUser(int id)
        {
            try
            {
                TeamMemberModel teamPerm = await _context.TeamMemberTable.Where(x => x.Id == id).Include(x => x.User).FirstOrDefaultAsync();
                TeamMemberModel _currentUserPerm = await _context.TeamMemberTable.FirstOrDefaultAsync(_x => _x.User.UserName == User.Identity.Name);
                if (teamPerm.User.UserName != User.Identity.Name && teamPerm.W_Perm < _currentUserPerm.W_Perm)
                {
                    _context.TeamMemberTable.Remove(teamPerm);
                    await _context.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return Forbid();
                }
            }
            catch { return new StatusCodeResult(500); }
        }
        
    }
}