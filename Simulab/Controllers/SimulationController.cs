﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Simulab.Models.Database_Models;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace Simulab.Controllers
{
    [Authorize]
    public class SimulationController : Controller
    {
        private SimulabContext _context = new SimulabContext();

        private readonly UserManager<UserModel> _userManager;
        public SimulationController(UserManager<UserModel> userManager)
        {
            _userManager = userManager;
        }
        /// <summary>
        /// Return simulation page
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Simulation()
        {
            return View();
        }

        /// <summary>
        /// Return configurator page
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Configurator()
        {
            return View(new SimulationConfigurationModel { UserId = _userManager.GetUserId(User) });
        }

        /// <summary>
        /// Submit configuration result to simulation dispatcher
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Configurator(SimulationConfigurationModel m)
        {
            if (ModelState.IsValid)
            {
                m.Date = DateTime.UtcNow;
                _context.SimConfTable.Add(m);
                await _context.SaveChangesAsync();
                return RedirectToAction("Result", new { id = m.Id });
            }
            else
            {
                return View(m);
            }
        }

        /// <summary>
        /// Return Model select component
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> ConfiguratorModelSelect()
        {
            return PartialView(await new ModelController(null).GetModelList(_userManager.GetUserId(User)));
        }

        /// <summary>
        /// return Initial Condition table
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> ConfiguratorICs(int id)
        {
            ModelItemModel m = await _context.ModelTable.FirstOrDefaultAsync(x => x.Id == id);
            return PartialView(m);
        }

        /// <summary>
        /// Return history page
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> History()
        {
            List<SimulationConfigurationModel> configs = _context.SimConfTable.Where(x => x.UserId == _userManager.GetUserId(User))
            .Include(x => x.Model)
            .Include(x => x.User).ToList();
            return View(configs);
        }

        /// <summary>
        /// Return result page according to simulation id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Result(int id)
        {
            SimulationResultModel m = await _context.SimResltTable.FindAsync(id);
            if (m == null)
            {                
                SimulationConfigurationModel config = await _context.SimConfTable.FindAsync(id);
                List<decimal> sim_settings = new List<decimal>();
                List<decimal> initconds = new List<decimal>();
                string parameters = "[2.1292,4.2584,4.2584,0.1185]";
                sim_settings.Add(config.Solver_StartTime);
                sim_settings.Add(config.Solver_EndTime);
                sim_settings.Add(config.Solver_Interval);
                sim_settings.Add(config.Solver_MaxStep);
                sim_settings.Add(config.Solver_Tighten);
                sim_settings.Add(config.Solver_RelativeTolerance);
                sim_settings.Add(config.Solver_AbsoluteTolerance);
                sim_settings.Add(config.Solver_Timeout);
                Dictionary<string, decimal> IC = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(config.InitialConditions);
                foreach (KeyValuePair<string, decimal> entry in IC)
                {
                    initconds.Add(entry.Value);
                }

                string postData = "{\"sim_settings\":" + JsonConvert.SerializeObject(sim_settings) + ", \"parameters\": " + parameters + ", " +
                    "\"initconds\":" + JsonConvert.SerializeObject(initconds) + "}";

                using (HttpClient client = new HttpClient())
                {
                    using (HttpResponseMessage response = await client.PostAsync("http://model2.simulab.io/", new StringContent("{}", Encoding.UTF8, "application/json")))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            m = new SimulationResultModel();
                            m.Title = (await _context.ModelTable.FindAsync(config.ModelId)).Name;
                            m.Description = config.Date.ToString();
                            string content = await response.Content.ReadAsStringAsync();
                            try
                            {
                                List<List<string>> lines = JsonConvert.DeserializeObject<List<List<string>>>(content);
                                List<List<string>> temp_data = new List<List<string>>();
                                int length = 0;
                                foreach (List<string> line in lines)
                                {
                                    length = line.Count;
                                    if (m.Data == null)
                                    {
                                        for (int i = 0; i < length; i++)
                                        {
                                            temp_data.Add(new List<string>());
                                            temp_data[i].Add(line[i]);
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 1; i < length; i++)
                                        {
                                            temp_data[i].Add(line[i]);
                                        }
                                    }
                                }
                                m.Data = new SimulationDataModel
                                {
                                    Data_x = temp_data[0].ToList(),
                                    Data_y = new List<List<string>>()
                                };
                                for (int i = 1; i < length; i++)
                                {
                                    m.Data.Data_y.Add(temp_data[i].ToList());
                                }
                                m.Result = JsonConvert.SerializeObject(m.Data);
                                _context.SimResltTable.Add(m);
                                await _context.SaveChangesAsync();
                            }
                            catch
                            {
                            }

                        }
                    }
                }
            }
            else
            {
                m.Data = JsonConvert.DeserializeObject<SimulationDataModel>(m.Result);
            }
            return View(m);

            /*
            SimulationResultModel m = new SimulationResultModel();
            m.Title = "Test";
            m.Description = "Description";
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string[] lines = System.IO.File.ReadAllLines(contentRootPath + @"/Contents/example_01_csv_no_roots.csv");
            int length = 0;
            List<List<string>> temp_data = new List<List<string>>();
            foreach (string line in lines)
            {
                string[] data = line.Split(',');
                length = data.Length;
                if (m.Data == null)
                {
                    for (int i = 0; i < length; i++)
                    {
                        temp_data.Add(new List<string>());
                        temp_data[i].Add(data[i]);
                    }
                }
                else
                {
                    for (int i = 1; i < length; i++)
                    {
                        temp_data[i].Add(data[i]);
                    }
                }
            }
            m.Data = new SimulationDataModel();
            m.Data.Data_x = temp_data[0].ToList();
            m.Data.Data_y = new List<List<string>>();
            for (int i = 1; i < length; i++)
            {
                m.Data.Data_y.Add(temp_data[i].ToList());
            }

            return View(m);*/
        }
    }
}