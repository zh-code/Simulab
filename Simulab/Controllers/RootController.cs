using System.Threading.Tasks;
using Simulab.Models;
using Simulab.Models.Database_Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Simulab.Controllers
{
  public class RootController : Controller
    {
        private readonly UserManager<UserModel> _userManager;
        private readonly SignInManager<UserModel> _signInManager;
        public RootController(UserManager<UserModel> userManager, SignInManager<UserModel> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        /// <summary>
        /// Get the login page
        /// </summary>
        /// <returns></returns>
        [Route("~/Login")]
        public IActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// Submit login request
        /// </summary>
        /// <param name="m"></param>
        /// <param name="returnUrl">Url to return after login</param>
        /// <returns></returns>
        [HttpPost("~/Login")]
        public async Task<IActionResult> Login(UserLoginModel m, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(m.Username, 
                    m.Password, m.RememberMe, lockoutOnFailure: false);
                /*
                if (result.RequiresTwoFactor)
                {
                    return RedirectToAction(nameof(SendCode), new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                }
                */
                if (result.IsLockedOut)
                {
                    ModelState.AddModelError(string.Empty, "User account locked out.");
                    return View("Lockout");
                }
                else if (result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(returnUrl)) return Redirect(returnUrl);
                }
                else
                {
                    ViewBag.ErrorMessage = "Incorrect Username or Password.";
                    return View(m);
                }
            }
            return RedirectToAction(nameof(ModelController.MView), "Model");
        }
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction(nameof(ModelController.MView), "Model");
        }

        /// <summary>
        /// Return register page
        /// </summary>
        /// <returns></returns>
        [Route("~/Register")]
        public IActionResult Register()
        {
            return View(new UserCreateModel());
        }

        /// <summary>
        /// Submit registration request
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPost("~/Register")]
        public async Task<IActionResult> Register(UserCreateModel m)
        {
            if (ModelState.IsValid)
            {
                var user = new UserModel { UserName = m.Username, Email = m.Email };
                var result = await _userManager.CreateAsync(user, m.Password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("View", "Model");
                }
            }
            return View(m);
        }
    }
}