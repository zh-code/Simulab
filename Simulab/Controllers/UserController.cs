﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Simulab.Models.Database_Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Simulab.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private SimulabContext _context = new SimulabContext();
        private readonly UserManager<UserModel> _userManager;
        public UserController(UserManager<UserModel> userManager)
        {
            _userManager = userManager;
        }

        /// <summary>
        /// Return account page
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Account()
        {
            UserModel user = await _userManager.FindByNameAsync(User.Identity.Name);
            return View(user);
        }

        /// <summary>
        /// Update user account
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Account(UserModel user)
        {
            if (ModelState.IsValid)
            {
                _context.UserTable.Update(user);
                await _context.SaveChangesAsync();
            }
            else
            {
                ViewBag.Message = "Input Invalid";
            }
            return View(user);
        }

        /// <summary>
        /// return user teams page
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Team()
        {
            UserModel user = await _context.UserTable.Where(x => x.UserName == User.Identity.Name)
                .Include(c => c.Teams).FirstOrDefaultAsync();
            foreach (TeamMemberModel m in user.Teams)
            {
                await _context.Entry(m).Reference(p => p.Team).LoadAsync();
            }
            return View(user);
        }

        /// <summary>
        /// Find user by email or username
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> FindUser(string filter)
        {
            UserModel user = await _context.UserTable.FirstOrDefaultAsync(x => x.UserName == filter || x.Email == filter);
            if (user != null)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}