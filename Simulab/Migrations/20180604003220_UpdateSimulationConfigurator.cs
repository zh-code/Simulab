﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Simulab.Migrations
{
    public partial class UpdateSimulationConfigurator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SimResltTable_SimConfTable_ConfigId",
                table: "SimResltTable");

            migrationBuilder.DropIndex(
                name: "IX_SimResltTable_ConfigId",
                table: "SimResltTable");

            migrationBuilder.AlterColumn<int>(
                name: "ConfigId",
                table: "SimResltTable",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Solver_Timeout",
                table: "SimConfTable",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<decimal>(
                name: "Solver_StartTime",
                table: "SimConfTable",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<decimal>(
                name: "Solver_EndTime",
                table: "SimConfTable",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<decimal>(
                name: "Retention",
                table: "SimConfTable",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<decimal>(
                name: "Data",
                table: "SimConfTable",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<decimal>(
                name: "Solver_Interval",
                table: "SimConfTable",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Solver_MaxStep",
                table: "SimConfTable",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Solver_Tighten",
                table: "SimConfTable",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateIndex(
                name: "IX_SimResltTable_ConfigId",
                table: "SimResltTable",
                column: "ConfigId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SimConfTable_ModelId",
                table: "SimConfTable",
                column: "ModelId");

            migrationBuilder.AddForeignKey(
                name: "FK_SimConfTable_ModelTable_ModelId",
                table: "SimConfTable",
                column: "ModelId",
                principalTable: "ModelTable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SimResltTable_SimConfTable_ConfigId",
                table: "SimResltTable",
                column: "ConfigId",
                principalTable: "SimConfTable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SimConfTable_ModelTable_ModelId",
                table: "SimConfTable");

            migrationBuilder.DropForeignKey(
                name: "FK_SimResltTable_SimConfTable_ConfigId",
                table: "SimResltTable");

            migrationBuilder.DropIndex(
                name: "IX_SimResltTable_ConfigId",
                table: "SimResltTable");

            migrationBuilder.DropIndex(
                name: "IX_SimConfTable_ModelId",
                table: "SimConfTable");

            migrationBuilder.DropColumn(
                name: "Solver_Interval",
                table: "SimConfTable");

            migrationBuilder.DropColumn(
                name: "Solver_MaxStep",
                table: "SimConfTable");

            migrationBuilder.DropColumn(
                name: "Solver_Tighten",
                table: "SimConfTable");

            migrationBuilder.AlterColumn<int>(
                name: "ConfigId",
                table: "SimResltTable",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "Solver_Timeout",
                table: "SimConfTable",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<int>(
                name: "Solver_StartTime",
                table: "SimConfTable",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<int>(
                name: "Solver_EndTime",
                table: "SimConfTable",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<int>(
                name: "Retention",
                table: "SimConfTable",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<int>(
                name: "Data",
                table: "SimConfTable",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.CreateIndex(
                name: "IX_SimResltTable_ConfigId",
                table: "SimResltTable",
                column: "ConfigId",
                unique: true,
                filter: "[ConfigId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_SimResltTable_SimConfTable_ConfigId",
                table: "SimResltTable",
                column: "ConfigId",
                principalTable: "SimConfTable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
