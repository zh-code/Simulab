﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Simulab.Migrations
{
    public partial class UpdateSimResultData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SimResltTable_SimConfTable_ConfigId",
                table: "SimResltTable");

            migrationBuilder.DropIndex(
                name: "IX_SimResltTable_ConfigId",
                table: "SimResltTable");

            migrationBuilder.DropColumn(
                name: "ConfigId",
                table: "SimResltTable");

            migrationBuilder.AddColumn<string>(
                name: "Result",
                table: "SimResltTable",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ResultId",
                table: "SimConfTable",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SimConfTable_ResultId",
                table: "SimConfTable",
                column: "ResultId",
                unique: true,
                filter: "[ResultId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_SimConfTable_SimResltTable_ResultId",
                table: "SimConfTable",
                column: "ResultId",
                principalTable: "SimResltTable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SimConfTable_SimResltTable_ResultId",
                table: "SimConfTable");

            migrationBuilder.DropIndex(
                name: "IX_SimConfTable_ResultId",
                table: "SimConfTable");

            migrationBuilder.DropColumn(
                name: "Result",
                table: "SimResltTable");

            migrationBuilder.DropColumn(
                name: "ResultId",
                table: "SimConfTable");

            migrationBuilder.AddColumn<int>(
                name: "ConfigId",
                table: "SimResltTable",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_SimResltTable_ConfigId",
                table: "SimResltTable",
                column: "ConfigId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SimResltTable_SimConfTable_ConfigId",
                table: "SimResltTable",
                column: "ConfigId",
                principalTable: "SimConfTable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
