﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Simulab.Migrations
{
    public partial class AddModelDisableColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SimResltTable_AspNetUsers_UserModelId",
                table: "SimResltTable");

            migrationBuilder.DropIndex(
                name: "IX_SimResltTable_UserModelId",
                table: "SimResltTable");

            migrationBuilder.DropColumn(
                name: "UserModelId",
                table: "SimResltTable");

            migrationBuilder.AddColumn<bool>(
                name: "Disabled",
                table: "ModelTable",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Disabled",
                table: "ModelTable");

            migrationBuilder.AddColumn<string>(
                name: "UserModelId",
                table: "SimResltTable",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SimResltTable_UserModelId",
                table: "SimResltTable",
                column: "UserModelId");

            migrationBuilder.AddForeignKey(
                name: "FK_SimResltTable_AspNetUsers_UserModelId",
                table: "SimResltTable",
                column: "UserModelId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
