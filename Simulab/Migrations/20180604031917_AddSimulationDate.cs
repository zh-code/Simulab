﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Simulab.Migrations
{
    public partial class AddSimulationDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Data",
                table: "SimConfTable");

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "SimConfTable",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "InitialConditions",
                table: "SimConfTable",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date",
                table: "SimConfTable");

            migrationBuilder.DropColumn(
                name: "InitialConditions",
                table: "SimConfTable");

            migrationBuilder.AddColumn<decimal>(
                name: "Data",
                table: "SimConfTable",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
