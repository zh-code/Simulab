﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Simulab.Migrations
{
    public partial class UpdateConfiguratorLifetimeType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Lifetime",
                table: "SimConfTable",
                nullable: false,
                oldClrType: typeof(string));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Lifetime",
                table: "SimConfTable",
                nullable: false,
                oldClrType: typeof(int));
        }
    }
}
