﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Simulab.Models
{
    public class UserCreateModel
    {
        /// <summary>
        /// The name of the user
        /// </summary>
        [Required(ErrorMessage = "Username is required")]
        public string Username { get; set; }

        /// <summary>
        /// The email address of the user
        /// </summary>
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        /// <summary>
        /// The password for the user
        /// </summary>
        [Required]
        [MinLength(8, ErrorMessage = "Password must be at least 8 characters")]
        [DataType(DataType.Password)]
        [PasswordPropertyText]
        public string Password { get; set; }

        /// <summary>
        /// The password confirmation
        /// </summary>
        [Required]
        [Compare("Password", ErrorMessage = "Password confirmation does not match password")]
        public string PasswordConfirmation { get; set; }
    }
}
