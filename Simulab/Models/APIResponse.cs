﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulab.Models
{
    public class APIResponse
    {
        /// <summary>
        /// Status
        /// </summary>
        public bool Ok { get; set; }

        /// <summary>
        /// API Message
        /// </summary>
        public string Message { get; set; }
    }
}
