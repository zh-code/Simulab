using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Simulab.Models.Database_Models
{
    public class TeamMemberModel
    {
        
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column(Order = 1)]
        public int Id { get; set; }

        [Required, Range(1, 7, ErrorMessage = "Permission value range from 1 to 7")]
        [Display(Name = "Read")]
        public int R_Perm{ get; set; }

        [Required, Range(1, 7, ErrorMessage = "Permission value range from 1 to 7")]
        [Display(Name = "Write")]
        public int W_Perm{ get; set; }

        [Required, Range(1, 7, ErrorMessage = "Permission value range from 1 to 7")]
        [Display(Name = "Execute")]
        public int X_Perm{ get; set; }

        public UserModel User { get; set; }
        public TeamModel Team { get; set; }
    }
}