﻿using Simulab.Models.Simulations.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Simulab.Models.Database_Models
{
    public class SimulationConfigurationModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column(Order = 1)]
        public int Id { get; set; }

        public string UserId { get; set; }
        public virtual UserModel User { get; set; }
        
        [Required, Display(Name = "Model ID")]
        public int ModelId { get; set; }

        public virtual ModelItemModel Model { get; set; }

        public DateTime Date { get; set; }

        [Required]
        public string InitialConditions { get; set; }

        #region Parameter Families
        [Required, Display(Name = "Start Time")]
        public float PF_StartTime { get; set; }
        [Required, Display(Name = "End Time")]
        public float PF_EndTime { get; set; }
        [Required, Display(Name = "Step")]
        public float PF_Step { get; set; }
        #endregion

        [Required, Display(Name = "Treatment Settings")]
        public SimConfigTreatmentSettingType TreatmentSetting { get; set; }

        [Required, Display(Name = "Target Environment")]
        public SimConfigTargetEnvironmentType TargetEnvironment { get; set; }

        [Required, Display(Name = "Target Language")]
        public SimConfigTargetLanguageType TargetLanguage { get; set; }

        #region Solver Settings
        [Required, Display(Name = "Start Time")]
        public decimal Solver_StartTime{ get; set; }

        [Required, Display(Name = "End Time")]
        public decimal Solver_EndTime { get; set; }

        [Required, Display(Name = "Interval")]
        public decimal Solver_Interval { get; set; }

        [Required, Display(Name = "Max Step")]
        public decimal Solver_MaxStep { get; set; }

        [Required, Display(Name = "Tighten")]
        public decimal Solver_Tighten { get; set; }

        [Required, Display(Name = "Timeout")]
        public decimal Solver_Timeout { get; set; }

        [Required, Display(Name = "Abolute Tolerance")]
        public decimal Solver_AbsoluteTolerance { get; set; }

        [Required, Display(Name = "Relative Tolerance")]
        public decimal Solver_RelativeTolerance { get; set; }
        #endregion

        #region Data Settings
        [Required]
        public decimal Retention { get; set; }

        [Required]
        public SimnConfigDataLifeType Lifetime { get; set; }
        #endregion

        #region User Settings
        [Required, Display(Name = "View Live Data")]
        public bool ViewLiveData { get; set; }

        [Required, Display(Name = "Receive Notification When Done")]
        public bool Notification { get; set; }
        #endregion

        public int? ResultId{ get; set; }        
        public virtual SimulationResultModel Result { get; set; }
    }
}
