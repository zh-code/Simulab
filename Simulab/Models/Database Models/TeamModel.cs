using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Simulab.Models.Database_Models
{
    public class TeamModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column(Order = 1)]
        public int Id { get; set; }
        [Required, Column(Order = 2)]
        public string Name { get; set; }
        [Required, Column(Order = 3)]
        public string Description { get; set; }

        public ICollection<TeamMemberModel> Users { get; set; }
        public ICollection<ModelItemModel> Models { get; set; }
    }
}