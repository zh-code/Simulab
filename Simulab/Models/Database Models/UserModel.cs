using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Simulab.Models.Database_Models
{
    [Table("AspNetUsers")] //EF required this table name to authenticate people... 
    public class UserModel : IdentityUser
    {
        [Required, Column(Order = 3)]
        public bool Active { get; set; }

        public virtual ICollection<TeamMemberModel> Teams { get; set; }

        public virtual ICollection<SimulationConfigurationModel> SimulationConfigurations { get; set; }

        public virtual ICollection<ModelItemModel> Models { get; set; }

    }
}