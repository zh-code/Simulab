﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Simulab.Models.Database_Models
{
    public class SimulationResultModel
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }

        public string Result{ get; set; }

        [NotMapped]
        public SimulationDataModel Data { get; set; }

        public SimulationConfigurationModel Config { get; set; }
    }

    public class SimulationDataModel
    {
        public List<string> Data_x { get; set; }
        public List<List<string>> Data_y { get; set; }
    }
}
