﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Simulab.Models.Database_Models
{
    public class SimulabContext : IdentityDbContext<UserModel>
    {
        public SimulabContext() { }

        public DbSet<UserModel> UserTable { get; set; }
        public DbSet<TeamMemberModel> TeamMemberTable { get; set; }
        public DbSet<TeamModel> TeamTable { get; set; }
        public DbSet<SimulationConfigurationModel> SimConfTable { get; set; }
        public DbSet<SimulationResultModel> SimResltTable { get; set; }
        public DbSet<ModelItemModel> ModelTable { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(@"Data Source=68.81.182.202;Initial Catalog=Simulab;Integrated Security=False;user ID=SimulabService;Password=YkC3FBz7nPekSAY0UROF");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
