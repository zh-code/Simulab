﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Simulab.Models.Database_Models
{
    public class ModelItemModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column(Order = 1)]
        public int Id { get; set; }
        [Required]
        public string CreatorId{ get;set; }
        public virtual UserModel Creator { get; set; }
        public int? TeamId{ get; set; }
        public virtual TeamModel Team { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime CreationTime { get; set; }
        [Required]
        public string Equations { get; set; }
        public string InitialConditions { get; set; }
        public string Structure { get; set; }
        public bool Disabled { get; set; }
        public int? PreviousChangeId{ get; set; }
        public virtual ModelItemModel PreviousChange { get; set; }

        public virtual ICollection<SimulationConfigurationModel> SimulationConfigurations { get; set; }
    }
}
