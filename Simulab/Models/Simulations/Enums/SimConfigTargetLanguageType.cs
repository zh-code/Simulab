﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulab.Models.Simulations.Enums
{
    public enum SimConfigTargetLanguageType
    {
        C,
        Fortran,
        Cuda
    }
}
